import { FETCH_ENTERPRISES_INFO, SET_ENTERPRISES_INFO, SET_MODAL_VISIBILITY, GET_ENTERPRISE_INFO, SET_ENTERPRISE_INFO, FILTER_ENTERPRISES, SET_FILTERED_ENTERPRISES } from './types'

export const fetchEnterprisesInfo = (onSuccess, onError) => ({
	type: FETCH_ENTERPRISES_INFO,
	onSuccess,
	onError
})

export const setEnterprisesInfo = (data) => ({
	type: SET_ENTERPRISES_INFO,
	data
})

export const setModalVisibility = (data) => ({
	type: SET_MODAL_VISIBILITY,
	data
})

export const getEnterpriseInfo = (data, onSuccess, onError) => ({
	type: GET_ENTERPRISE_INFO,
	data,
	onSuccess,
	onError
})

export const setEnterpriseInfo = (data) => ({
	type: SET_ENTERPRISE_INFO,
	data
})

export const filterEnterprises = (data, onSuccess, onError) => ({
	type: FILTER_ENTERPRISES,
	data,
	onSuccess,
	onError
})

export const setFilteredEnterprises = (data) => ({
	type: SET_FILTERED_ENTERPRISES,
	data
})