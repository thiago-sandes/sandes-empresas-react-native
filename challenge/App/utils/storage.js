import { AsyncStorage } from 'react-native'
const USER_TOKEN = 'USER_TOKEN'
export const setCustomHeader = async (value) => {
	const customHeaderValues = {'access-token': value['access-token'], 'client': value['client'], 'uid': value['uid']}
  try {
		await AsyncStorage.setItem('X-Custom-Header', JSON.stringify(customHeaderValues))
    return true
  } catch (error) {
    return false
  }
}

export const clearCustomHeader = async () => setCustomHeader('')

export const getCustomHeader = async () => {
  try {
    const customHeaderObj = JSON.parse(await AsyncStorage.getItem('X-Custom-Header'))
    if (customHeaderObj !== null) {
      return customHeaderObj
    }
    return ''
  } catch (error) {
    return ''
  }
}

export const setUserInfo = async (value) => {
  try {
		await AsyncStorage.setItem('UserInfo', JSON.stringify(value))
    return true
  } catch (error) {
    return false
  }
}

export const clearUserInfo = async () => setUserInfo('')

export const getUserInfo = async () => {
  try {
    const userInfoObj = JSON.parse(await AsyncStorage.getItem('UserInfo'))
    if (userInfoObj !== null) {
      return userInfoObj
    }
    return ''
  } catch (error) {
    return ''
  }
}

export const saveToken = async (value) => {
  try {
    await AsyncStorage.setItem(`@${USER_TOKEN}:key`, `${value}`)
    return true
  } catch (error) {
    return false
  }
}

export const clearToken = async () => saveToken('');

export const getToken = async () => {
  try {
    const value = await AsyncStorage.getItem(`@${USER_TOKEN}:key`)
    if (value !== null) {
      return value
    }
    return ''
  } catch (error) {
    return ''
  }
}