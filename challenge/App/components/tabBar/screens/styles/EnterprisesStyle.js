import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  content: {
    flex: 1,                            
    justifyContent: 'center',                     
    backgroundColor: '#C2185B'         
  },
	textButton: {
		color: '#e01e69',
		fontSize: 20,
		fontWeight: 'bold'
	},
	enterprisesContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
		width: '80%',
		backgroundColor: '#FFF',
		marginVertical: '4%',
		marginHorizontal: '6%',
		paddingVertical: '6%',
		borderWidth: 4,
		borderRadius: 20,
		borderColor: '#e01e69',
		borderBottomWidth: 0,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2
	}
})