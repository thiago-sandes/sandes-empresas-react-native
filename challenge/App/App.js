import React, { Component } from 'react'
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import allReducers from './reducers';
import AppNavigation from './navigation/AppNavigation';
import creatSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/rootSaga';
import { StatusBar } from "react-native";

const sagaMiddleware = creatSagaMiddleware()
let store = createStore(allReducers, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(rootSaga)

class App extends Component {
  render () {
    return (
			<>
				<StatusBar translucent backgroundColor={'transparent'} />
				<Provider store={store}>
					<AppNavigation />
				</Provider>
			</>
    )
  }
}

export default App