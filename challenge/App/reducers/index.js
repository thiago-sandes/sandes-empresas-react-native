import { combineReducers } from 'redux'
import loginReducers from './loginReducers'
import enterprisesReducers from './enterprisesReducers'

const allReducers = combineReducers({
	login: loginReducers,
	home: enterprisesReducers
})

export default allReducers