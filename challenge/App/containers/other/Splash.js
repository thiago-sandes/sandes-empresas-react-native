import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { getToken, getCustomHeader, getUserInfo, clearToken, clearCustomHeader, clearUserInfo  } from '../../utils/storage'
import * as enterprisesActions from '../../actions/enterprisesActions'
import * as loginActions from '../../actions/loginActions'
import { NavigationActions, StackActions } from 'react-navigation'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from './styles/SplashStyle'

class Splash extends Component {

	componentDidMount() {
		setTimeout(() => {
			this.checkSignInStatus()
		}, 1000)
	}

	onSuccess = async (data) => {
		const { navigation } = this.props
		const customHeader = await getCustomHeader()
		const userInfo = await getUserInfo()
		
		let routeName = 'login'
		if (data) {
			routeName = 'home';
			this.props.actions.home.setEnterprisesInfo(data.data.enterprises)
			this.props.actions.login.setUserInfo(userInfo)
			this.props.actions.login.setCustomHeader(customHeader)
		}
		setTimeout(() => {
			const resetAction = StackActions.reset({
				index: 0,
				actions: [
						NavigationActions.navigate({ routeName })
				]
			})
			navigation.dispatch(resetAction)
		}, 300)
	}

	onError = (error) => {
		try {
			clearToken()
			clearCustomHeader()
			clearUserInfo()
			const { navigation } = this.props
			const resetAction = StackActions.reset({
				index: 0,
				actions: [
					NavigationActions.navigate({ routeName: 'login' })
				]
			})
			navigation.dispatch(resetAction)
		} catch (e) {
			this.setState({})
		}
	}

	checkSignInStatus() {
		getToken().then((token) => {
			if (token && token.length > 0) {
				this.props.actions.home.fetchEnterprisesInfo(this.onSuccess, this.onError)
			} else {
				const { navigation } = this.props
				const resetAction = StackActions.reset({
					index: 0,
					actions: [
							NavigationActions.navigate({ routeName: 'login' })
					]
				})
				navigation.dispatch(resetAction)
			}
		})
	}
	
	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.text}>CHALLENGE ACCEPTED! :)</Text>
			</View>
		)
	}
}

const mapDispatchToProps = (dispatch) => ({
	actions: {
		home: bindActionCreators(enterprisesActions, dispatch),
		login: bindActionCreators(loginActions, dispatch)
	}
})

export default connect(null, mapDispatchToProps)(Splash)


