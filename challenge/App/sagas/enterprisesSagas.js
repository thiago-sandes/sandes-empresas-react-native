import { call, takeLatest } from 'redux-saga/effects'
import * as types from '../actions/types'
import * as enterprisesApis from '../services/enterprisesApis'

export function* fetchEnterprisesInfo(action) {
	try {
		const data = yield call(enterprisesApis.fetchEnterprisesInfo)
		action.onSuccess(data)
	} catch (error) {
		action.onError(error)
	}
}

export function* watchFetchEnterprisesInfo() {
	yield takeLatest(types.FETCH_ENTERPRISES_INFO, fetchEnterprisesInfo)
}

export function* getEnterpriseInfo(action) {
	try {
		const data = yield call(enterprisesApis.getEnterpriseInfo, action.data)
		action.onSuccess(data.data.enterprise)
	} catch (error) {
		action.onError(error)
	}
}

export function* watchGetEnterpriseInfo() {
	yield takeLatest(types.GET_ENTERPRISE_INFO, getEnterpriseInfo)
}

export function* filterEnterprises(action) {
	try {
		const data = yield call(enterprisesApis.filterEnterprises, action.data)
		action.onSuccess(data.data.enterprises)
	} catch (error) {
		action.onError(error)
	}
}

export function* watchFilterEnterprises() {
	yield takeLatest(types.FILTER_ENTERPRISES, filterEnterprises)
}