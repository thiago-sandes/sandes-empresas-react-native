import React, {useState, useEffect} from 'react'
import { View, KeyboardAvoidingView, Image, TextInput, TouchableOpacity, Text, Animated, Keyboard, ImageBackground, ActivityIndicator } from 'react-native'
import styles from './styles/LoginStyle'
import { animationParameters, animationInView, keyboardDidShow, keyboardDidHide, logoWidth, logoHeight } from '../../helpers/loginHelpers'
import backgroundImage from '../../assets/login-background.jpg'
import ioasysLogo from '../../assets/logo_ioasys.png'
import stylesConstants from '../../constants/stylesConstants'

export default function LoginScreen(props){
  
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
  const [offset] = useState(new Animated.ValueXY({x: 0, y: 95}))
  const [opacity] = useState(new Animated.Value(0))
  const [logo] = useState(new Animated.ValueXY({x: logoWidth, y: logoHeight}))
  
  useEffect(() => {
		KeyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => { keyboardDidShow(logo) })
		KeyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => { keyboardDidHide(logo) })
		
		Animated.parallel(animationParameters(offset, opacity)).start()
  }, [])
  
  return (
	  <ImageBackground style={ styles.imgBackground } 
		  resizeMode='cover'
			source={backgroundImage}
		>
			<KeyboardAvoidingView style={styles.background}>
				<View style={styles.containerLogo}>
					<Animated.Image
						style={{width: logo.x, height: logo.y}}
						source={ioasysLogo}
					/>
				</View>
			
				{ props.fetching && (
					<ActivityIndicator size={stylesConstants.loading.size} color={stylesConstants.loading.color} />
				)}
			
				<Animated.View 
					style={[styles.container, animationInView(offset, opacity)]}
				>
				
					<TextInput
						style={styles.input}
						placeholder="Email"
						autoCorrect={false}
						autoCapitalize="none"
						onChangeText={(value) => setEmail(value)}
					/>
					
					<TextInput
						style={styles.input}
						placeholder="Password"
						secureTextEntry
						autoCorrect={false}
						onChangeText={(value) => setPassword(value)}
					/>
					
					<TouchableOpacity style={styles.btnSubmit} onPress={() => { 
						props.login({ email: email, password: password })
						Keyboard.dismiss()
					}}>
						<Text style={styles.submitText}> Login </Text>
					</TouchableOpacity>
					
				</Animated.View>
			</KeyboardAvoidingView>
		</ImageBackground>
  )
}