![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações necessárias para execução do projeto Empresas.

---

### Agradecimentos
* Agradeço ao elaborador do desafio! Pude praticar o desenvolvimento de **código limpo e elegante** :)

### Considerações ###
* Demandei um bom tempo organizando o código de forma a componentizar o que pude. 
* Nesse sentido, me esforcei para seguir boas práticas de programação, como a do código limpo (Clean Code), propagado pelo caríssimo **Martin Fowler**.
* Me atentei a aplicar padrões no desenvolvimento, tanto na parte da organização do código quanto no design da aplicação.
* Garanti que tudo está funcionando corretamente no que tange ao aplicativo. Portanto, me esforcei em dobro para garantir que algum problema que possa ocorrer nele não seja por um erro meu, mas da API.
* Acredito que para uma aplicação ter qualdidade, ela precisa ser **clean e elegante**. Segui essa premissa nesse projeto.

### Informações Importantes
* **O `README.md` deve conter uma pequena justificativa de cada biblioteca adicionada ao projeto como dependência.**
	* "axios": "^0.19.2", foi usado para requisições mais eficientes e com menos código;
    * "react": "16.11.0", dependência padrão;
    * "react-native": "0.62.1", dependência padrão;
	* "react-navigation": "^2.3.1", foi usado para transição entre telas e controle de fluxo da aplicação; 
    * "react-redux": "^7.2.0", foi usado para gerenciar o estado do aplicativo;
    * "redux": "^4.0.5", foi usado para gerenciar o estado do aplicativo;
    * "redux-saga": "^1.1.3", foi usado para maior controle das operações assíncronas.
	
* **O `README.md` do projeto deve conter instruções de como executar a aplicação**
	* Sobre o ambiente:
		* Node version: 10.16.0;
		* Yarn version: 1.22.0;
		* Java version: 1.8.0_152.
		
	* Sobre o aplicativo:
		* Faça o clone do projeto: `git clone https://bitbucket.org/thiago-sandes/sandes-empresas-react -native.git`;
		* Instale as dependências com o comando `Yarn` ou `Yarn install`;
		* Siga os passos para emular uma aplicação React Native, direto da documentação oficial, por meio de um emulador Android:
			* https://reactnative.dev/docs/running-on-device;
		* Se você já estiver com o dispositivo configurado, segundo a documentação oficial, execute `npx react-native run-android`.
		* Veja que beleza: o aplicativo irá ser executado no seu dispositivo! :)
		
### O que será avaliado?
* **[Ok]** A ideia com este teste é ter um melhor entendimento das suas habilidades com Javascript e React Native. Mas de qualquer forma, um layout bonito e com boa usabilidade é **MUITO** bem vindo.
- **[Ok]** A qualidade e desempenho do seu código
- **[Ok]** Sua capacidade de organizar o código
- **[Ok]** Capacidade de tomar decisões

### Escopo do Projeto
* **[Ok]** O Login e acesso de Usuário já registrados
	* Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers:
		* `access-token`;
		* `client`;
		* `uid`;
		
	* Para ter acesso às demais APIs precisamos enviar esses 3 (três) custom headers para a API autorizar a requisição;
	
* **[Ok]** Endpoints disponíveis:
	* Listagem de Empresas: `/enterprises`
	* Detalhamento de Empresas: `/enterprises/{id}`
	* Filtro de Empresas por nome e tipo: `/enterprises?enterprise_types={type}&name={name}`
	
* **[Ok]** Gostaríamos que todos os três endpoints disponibilizados fossem utilizados.

### Dados para Teste ###
* Servidor: http://empresas.ioasys.com.br
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234