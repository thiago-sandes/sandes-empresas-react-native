import * as types from '../actions/types'
import appState from '../constants/initialState'

const enterprisesReducer = (state = appState.home, action) => {
	switch (action.type) {
		case types.SET_ENTERPRISES_INFO:
			return { ...state, ...{ enterprisesInfo: action.data } }
		case types.SET_MODAL_VISIBILITY:
			return { ...state, ...{ modalVisibility: action.data } }
		case types.SET_ENTERPRISE_INFO:
			return { ...state, ...{ enterpriseInfo: action.data } }
		case types.SET_FILTERED_ENTERPRISES:
			return { ...state, ...{ filteredEnterprises: action.data } }
		default:
			return state
	}
}
export default enterprisesReducer