export default{
	filter: {
		placeholderTextColor: 'rgba(0,0,0, 0.2)'
	},
	floatingButton: {
		hitSlop: {
			left: 20,
			right: 20,
			top: 20,
			bottom: 20,
		}
	},
	loading: {
		size: 'large',
		color: '#2196F3'
	},
	touchable: {
		color: '#2196F3'
	}
}