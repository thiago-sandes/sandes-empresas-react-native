import { StyleSheet } from 'react-native'
import stylesConstants from '../../../constants/stylesConstants'

export default StyleSheet.create({
  background:{
    flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'transparent'
  },
  containerLogo:{
    flex: 1,
		justifyContent: 'center'
  },
  container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		width: '90%',
		paddingBottom: '10%'
  },
  input: {
		backgroundColor: '#FFF',
		width: '90%',
		marginBottom: 15,
		color: '#222',
		fontSize: 17,
		borderRadius: 7,
		padding: 10
  },
  btnSubmit: {
		backgroundColor: stylesConstants.touchable.color,
		width: '90%',
		height: 45,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 7
  },
  submitText: {
		color: '#FFF',
		fontSize: 18,
		bottom: '3%'
  },
  imgBackground: {
	  width: '100%',
    height: '100%',
    flex: 1 
	}
})
