import React, { useState } from 'react'
import { Text, View, FlatList, TouchableOpacity, Alert } from 'react-native'
import FloatingButton from '../../floatingButton/FloatingButton'
import Modal from '../../modal/EnterpriseInfoModal'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as enterprisesActions from '../../../actions/enterprisesActions'
import styles from './styles/EnterprisesStyle'




function Enterprises(props){
	const { enterpriseInfo, modalVisibility} = props
	const { enterprisesInfo } = props.data
	
	const [ fetching, setFetching ] = useState(false)

	const onSuccess = async (data) => {
		setFetching(false)
		await props.actions.enterprise.setEnterpriseInfo(data)
	}
	
	const onError = (error) => {
		setFetching(false)
		Alert.alert('Error', 'An error occurred while fetching information from the server.')
	}
	return (
		<>
			<View style={styles.content}>
				{ props.data && (
					<FlatList
						data={enterprisesInfo}
						showsVerticalScrollIndicator={false}
						keyExtractor={(x, i) => i.toString()}
						renderItem={({ item }) =>
							<TouchableOpacity onPress={() => { 
								setFetching(true)
								props.actions.enterprise.getEnterpriseInfo(item.id, onSuccess, onError)
								props.actions.enterprise.setModalVisibility(!modalVisibility) 
							}}>	
								<View style={styles.enterprisesContainer}>
									<Text style={styles.textButton}>
										{`${item.enterprise_name}`}
									</Text>
								</View>
							</TouchableOpacity>
						}
							
					/>
				)}
			</View>
			<FloatingButton logout={props.logout} />
			<Modal data={enterpriseInfo} fetching={fetching}/>
		</>
	)
}

const mapStateToProps = (state) => ({
	modalVisibility: state.home.modalVisibility,
	enterpriseInfo: state.home.enterpriseInfo
})

const mapDispatchToProps = (dispatch) => {
	return {
			actions: {
					enterprise: bindActionCreators(enterprisesActions, dispatch)
			}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Enterprises)