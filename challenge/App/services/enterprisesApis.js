import { api } from './api'
import setCustomHeaders from '../helpers/setCustomHeaders'

export const fetchEnterprisesInfo = async () => {
	await setCustomHeaders()
	return api.get('/enterprises')
}

export const getEnterpriseInfo = async (id) => {
	await setCustomHeaders()
	return api.get(`/enterprises/${id}`)
}

export const filterEnterprises = async (name) => {
	await setCustomHeaders()
	return api.get(`/enterprises?name=${name}`)
}