import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import styles from './styles/TabStyle'

export default function Tab(props){
	
	const [activeTab, setActiveTab] = useState(0)
  const { children } = props
	
	return (
		<View style={styles.container}>
			
			<View style={styles.tabsContainer}>
				{children.map(({ props: { title } }, index) =>
					
					<TouchableOpacity
						style={[  
							styles.tabContainer,
							index === activeTab ? styles.tabContainerActive : []
						]}
						onPress={() => setActiveTab(index)}
						key={index}
					>
						
						<Text style={styles.tabText}>
							{title}
						</Text>
						
					</TouchableOpacity>
					
				)}
			</View>

			<View style={styles.contentContainer}>
				{children[activeTab]}
			</View>
			
		</View>
	)
}

