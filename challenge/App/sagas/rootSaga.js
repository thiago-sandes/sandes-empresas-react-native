import { all } from 'redux-saga/effects'
import { watchLogin } from './loginSagas'
import { watchFetchEnterprisesInfo, watchGetEnterpriseInfo, watchFilterEnterprises } from './enterprisesSagas'

function* rootSaga() {
	yield all([
		watchLogin(),
		watchFetchEnterprisesInfo(),
		watchGetEnterpriseInfo(),
		watchFilterEnterprises()
	])
}
export default rootSaga