import { LOGIN, SET_USER_INFO, SET_CUSTOM_HEADER } from './types'

export const login = (params, onSuccess, onError) => ({
	type: LOGIN,
	params,
	onSuccess,
	onError
})
export const setUserInfo = (data) => ({
	type: SET_USER_INFO,
	data
})

export const setCustomHeader = (data) => ({
	type: SET_CUSTOM_HEADER,
	data
})