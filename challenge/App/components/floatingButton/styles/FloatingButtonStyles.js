import { StyleSheet } from "react-native"

export default StyleSheet.create({
	bigBubble: {
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'flex-end',
		margin: 22,
		backgroundColor: '#e01e69',
		height: 60,
		width: 60,
		borderRadius: 100 / 2,
		borderWidth: 2,
		position: 'absolute',
		bottom: '0%',
		right: '0%',
		borderColor: 'rgba(0,0,0, 0.02)',
		borderTopWidth: 0,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2
	},
	icon:{
		tintColor: 'white',
		alignSelf: 'center',
		bottom: '0%',
		top: '0%',
		left: '5%',
		right: '0%'
	}
})

