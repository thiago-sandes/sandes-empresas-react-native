import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
  },

  tabsContainer: {
    flexDirection: 'row',
    paddingTop: 30,
  },

  tabContainer: {
    flex: 1,
    paddingVertical: 15,
    borderBottomWidth: 3,
    borderBottomColor: 'transparent',
  },

  tabContainerActive: {
    borderBottomColor: '#FFFFFF',
  },

  tabText: {
    color: '#FFFFFF',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  contentContainer: {
    flex: 1
  }
})