import { Animated, Dimensions } from 'react-native'

export const logoWidth = ( (Dimensions.get('window').width) * 90 / 100 )
export const logoHeight = ( (Dimensions.get('window').height) * 17 / 100 )

export function animationParameters(offset, opacity){
  return([ 
    Animated.spring(offset, {
			toValue: 8,
			speed: 4,
			bounciness: 20,
			useNativeDriver: false
    }),
    Animated.timing(opacity, {
      toValue: 1,
      duration: 200,
      useNativeDriver: false
    })
  ])
}

export function animationInView(offset, opacity){
  return({
    opacity: opacity,
		transform: [
			{ translateY: offset.y }
		]
  })
}

export function keyboardDidShow(logo){
  Animated.parallel([
    Animated.timing(logo.x, {
			toValue: ( (Dimensions.get('window').width) * 50 / 100 ),
			duration: 100,
			useNativeDriver: false
		}),
		Animated.timing(logo.y, {
			toValue: ( (Dimensions.get('window').width) * 15 / 100 ),
			duration: 100,
			useNativeDriver: false
		})
  ]).start()
}
  
export function keyboardDidHide(logo){
  Animated.parallel([
    Animated.timing(logo.x, {
			toValue: ( (Dimensions.get('window').width) * 90 / 100 ),
			duration: 100,
			useNativeDriver: false
		}),
		Animated.timing(logo.y, {
			toValue: ( Dimensions.get('window').height * 17 / 100 ),
			duration: 100,
			useNativeDriver: false
		})
  ]).start()
}