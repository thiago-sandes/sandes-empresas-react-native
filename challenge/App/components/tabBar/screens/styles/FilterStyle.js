import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  content: {
    flex: 1,                            
    justifyContent: 'center',           
    backgroundColor: '#C2185B'      
  },

 
	textButton: {
		color: '#e01e69',
		fontSize: 20,
		fontWeight: 'bold'
	},
	
	searchBarcontainer: {
		height: 45,
		backgroundColor: 'rgb(233, 30, 99)',
		flexDirection: 'row',
		alignItems: 'center',
		borderWidth: 2,
		borderColor: 'rgba(0,0,0, 0.05)'
	},
	
	searchBox: {
		height: 40,
		backgroundColor: 'rgba(0,0,0, 0.05)',
		flex: 1,
		justifyContent: 'space-around',
		flexDirection: 'row',
		marginLeft: '2%',
		borderRadius: 55,
		borderTopWidth: 0,
		borderColor: 'rgba(0,0,0, 0.1)',
		borderWidth: 2,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2
	},
	
	clearTextTouchable: {
		alignSelf: 'center'		
	},
	
	searchButtonTouchable: {
		marginHorizontal: '2%',
		alignSelf: 'center'
	},
	
	icon: {
		tintColor: 'white'
	},
	
	textInput: {
		width: '80%',
		color: 'white'
	},
	
	textButton: {
		color: '#e01e69',
		fontSize: 20,
		fontWeight: 'bold'
	},

	enterprisesContainer: {
		flex: 1,
		width: '80%',
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFF',
		marginVertical: '4%',
		marginHorizontal: '6%',
		paddingVertical: '6%',
		borderWidth: 4,
		borderRadius: 20,
		borderColor: '#e01e69',
		borderBottomWidth: 0,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2
	}
	
})