import React, { Component } from 'react';
import { View } from 'react-native';
import Tab from './Tab'
import Home from './screens/Home'
import Enterprises from './screens/Enterprises'
import Filter from './screens/Filter'
import styles from './styles/TabBarStyle'

export default function TabBar(props){
	return (
		<View style={styles.container}>
			<Tab>

				<Home logout={props.logout} title="HOME" data={props.data}/>
				<Enterprises logout={props.logout} title="ENTERPRISES" data={props.data}/>
				<Filter logout={props.logout} title="FILTER" data={props.data}/>
 
			</Tab>
		</View>
	)
}

