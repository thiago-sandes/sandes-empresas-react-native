import React from 'react'
import { TouchableOpacity, View, Image } from 'react-native'
import styles from './styles/FloatingButtonStyles'
import logoutIcon from '../../assets/logout.png'
import stylesConstants from '../../constants/stylesConstants'

export default function FloatingButton(props) {
	return (
		<View>
			<View style={styles.bigBubble}>
				<TouchableOpacity
					hitSlop={stylesConstants.floatingButton.hitSlop}
					onPress={() => props.logout()}
				>
				<Image
					style={styles.icon}
					source={logoutIcon}
				/>
				</TouchableOpacity>
			</View>
		</View>
	)
}