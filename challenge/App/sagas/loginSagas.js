import { call, takeLatest } from 'redux-saga/effects'
import * as types from '../actions/types'
import * as loginApis from '../services/loginApis'
import { setToken } from '../services/api'
import { setCustomHeader as setCustomHeaderStorage, setUserInfo as setUserInfoStorage } from '../utils/storage'

export function* login(action) {
	try {
		const data = yield call(loginApis.login, action.params)
		
		setToken(data.headers['access-token'] || '')
		setCustomHeaderStorage(data.headers)
		setUserInfoStorage(data.data.investor)
		
		action.onSuccess(data)
	} catch (error) {
		action.onError(error)
	}
}

export function* watchLogin() {
	yield takeLatest(types.LOGIN, login)
}