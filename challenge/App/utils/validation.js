export const validations = {
	email: {
		presence: {
			title: 'Email not provided',
			message: 'Please enter email.'
		},
		format: {
			pattern: /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}(.[a-zA-Z]{2,3})?$/,
			title: 'Invalid email format',
			message: 'Email entered is not in the correct format.'
		}
	},
	password: {
		presence: {
			title: 'Password not provided',
			message: 'Please enter a password.'
		}
	}
}

export const validate = (nameField, value) => {
	let result = { isError: false, titleError: '', messageError: '' }
	if (validations.hasOwnProperty(nameField)) {
		let v = validations[nameField]
		if (value == '' || value === null) {
			result = { isError: true, titleError: v.presence.title, messageError: v.presence.message }
		} else if (v.hasOwnProperty('format') && !v.format.pattern.test(value)) {
			result = { isError: true, titleError: v.format.title, messageError: v.format.message}
		} else {
			result.isError = false
		}
	} else {
		result.isError = false
	}
	return result
}