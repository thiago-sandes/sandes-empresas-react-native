import React, { Component } from 'react'
import { View } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { clearToken, clearCustomHeader, clearUserInfo } from '../../utils/storage'
import { NavigationActions, StackActions } from 'react-navigation'
import * as enterprisesActions from '../../actions/enterprisesActions'
import TabBar from '../../components/tabBar/TabBar'
import styles from './styles/HomeContainerStyle'

class HomeContainer extends Component {
	render() {
		return (
			<View style={styles.container}>
				<TabBar logout={this.logout} data={this.props.data}/>
			</View>
		)
	}
		
	onSuccess = async (data) => {
		this.props.actions.home.setEnterprisesInfo(data.data.enterprises)
	}

	onError = (error) => {
		try {
			clearToken()
			clearCustomHeader()
			clearUserInfo()
			const { navigation } = this.props
			const resetAction = StackActions.reset({
				index: 0,
				actions: [
						NavigationActions.navigate({ routeName: 'login' })
				]
			})
			navigation.dispatch(resetAction)
		} catch (e) {
			this.setState({})
		}
	}
	
	logout = () => {
		clearToken()
		clearCustomHeader()
		clearUserInfo()
		const { navigation } = this.props
		const resetAction = StackActions.reset({
			index: 0,
			actions: [
					NavigationActions.navigate({ routeName: 'login' })
			]
		})
		navigation.dispatch(resetAction);
	}
		
	componentDidMount(){
		{ !this.props.data.enterprisesInfo[0] && (
			this.props.actions.home.fetchEnterprisesInfo(this.onSuccess, this.onError)
		)}
	}
}

const mapStateToProps = (state) => ({
	data: {
		userInfo: state.login.userInfo,
		customHeader: state.login.customHeader,
		enterprisesInfo: state.home.enterprisesInfo
	}
})

const mapDispatchToProps = (dispatch) => ({
	actions: {
		home: bindActionCreators(enterprisesActions, dispatch)
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)