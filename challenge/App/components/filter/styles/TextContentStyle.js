import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  header: {
    margin: 10,                         
    color: '#FFFFFF',                   
    fontFamily: 'Avenir',               
    fontSize: 26                       
  },

  text: {
    marginHorizontal: 20,               
    color: 'rgba(255, 255, 255, 0.75)', 
    textAlign: 'center',                
    fontFamily: 'Avenir',
    fontSize: 18
  },
	
	textContentContainer: {
		alignItems: 'center'
	}
	
})