export default{
	filter: {
		initialTitle: 'Easy to use',
		initialDescription: 'Enter the enterprise name and we\'ll find it for you',
		noDataTitle: 'Oops ...',
		noDataDescription: 'We didn\'t find the data related to your query'
	}
}