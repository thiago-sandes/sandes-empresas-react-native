import { createStackNavigator } from 'react-navigation'
import LoginContainer from '../containers/login/LoginContainer'
import HomeContainer from '../containers/home/HomeContainer'
import Splash from '../containers/other/Splash'

const AppStackNavigator = createStackNavigator(
{
	splash: { screen: Splash },
	login: { screen: LoginContainer },
	home: { screen: HomeContainer }
}, {
	headerMode: 'none'
})

export default AppStackNavigator