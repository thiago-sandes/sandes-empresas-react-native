import { StyleSheet } from "react-native"

export default StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
		width: '90%',
		height: '70%',
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
	scrollView: {
		alignSelf: 'stretch'
	},
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
		alignSelf: 'stretch'
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
	titleTextStyle: {
		fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
		marginVertical: '3%' 
  },
  modalText: {
		fontSize: 16,
    marginVertical: '3%',
    textAlign: "center"
  }
})