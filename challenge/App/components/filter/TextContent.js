import React from 'react'
import { View, Text } from 'react-native'
import styles from './styles/TextContentStyle'

export default function TextContent(props){
	return (
		<View style={styles.textContentContainer}>
			<Text style={styles.header}>
				{props.title}
			</Text>
			<Text style={styles.text}>
				{props.description}
			</Text>
		</View>
	)
}