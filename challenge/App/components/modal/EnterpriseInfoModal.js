import React, { Component, useState } from "react"
import { Modal, Text, TouchableHighlight, View, ScrollView, ActivityIndicator } from "react-native"
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as enterprisesActions from '../../actions/enterprisesActions'
import styles from './styles/EnterpriseInfoModalStyle'
import stylesConstants from '../../constants/stylesConstants'


const EnterpriseInfoModal = (props) => {
	const { modalVisibility } = props
	const enterpriseInfo = props.data
	const fetching = props.fetching

  return (
		<>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisibility}
        onRequestClose={() => {
          props.actions.enterprise.setModalVisibility(!modalVisibility)
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
						
							<ScrollView style={styles.scrollView}>
							{ fetching ? (
								<ActivityIndicator size={stylesConstants.loading.size} color={stylesConstants.loading.color} />
							) : (
								<>
									<Text style={styles.titleTextStyle}>Enterprise name</Text>
									<Text style={styles.modalText}>{enterpriseInfo['enterprise_name']}</Text>
									<Text style={styles.titleTextStyle}>Description</Text>
									<Text style={styles.modalText}>{enterpriseInfo['description']}</Text>
								</>
							)}
							</ScrollView>
						
						
						<TouchableHighlight
							style={{ ...styles.openButton, backgroundColor: stylesConstants.touchable.color }}
							onPress={() => {
								props.actions.enterprise.setModalVisibility(!modalVisibility)
							}}
						>
							<Text style={styles.textStyle}>Close Modal</Text>
						</TouchableHighlight>
          </View>
        </View>
      </Modal>
		</>
  )
}

const mapStateToProps = (state) => ({
	modalVisibility: state.home.modalVisibility
})

const mapDispatchToProps = (dispatch) => {
	return {
		actions: {
			enterprise: bindActionCreators(enterprisesActions, dispatch)
		}
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(EnterpriseInfoModal)
