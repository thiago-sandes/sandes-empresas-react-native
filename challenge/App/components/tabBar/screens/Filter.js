import React, { useState } from 'react'
import { Text, View, TextInput, TouchableOpacity, Image, Alert, Keyboard, ActivityIndicator, FlatList } from 'react-native'
import FloatingButton from '../../floatingButton/FloatingButton'
import TextContent from '../../../components/filter/TextContent'
import Modal from '../../modal/EnterpriseInfoModal'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as enterprisesActions from '../../../actions/enterprisesActions'
import styles from './styles/FilterStyle'
import searchIcon from '../../../assets/search.png'
import cancelIcon from '../../../assets/cancel.png'
import stylesConstants from '../../../constants/stylesConstants'
import textsConstants from '../../../constants/textsConstants'

function Filter(props){
	const { filteredEnterprises, modalVisibility, enterpriseInfo } = props.data
	const [ cancelSearch, setCancelSearch ] = useState(false)
	const [ searchText, setSearchText ] = useState('')
	const [ fetching, setFetching ] = useState(false)
	const [ modalFetching, setModalFetching ] = useState(false)
	const [ hasFilteredData, setHasFilteredData ] = useState(true)
	const hasData = Object.entries(filteredEnterprises).length !== 0

	
	
	const onSuccess = async (data) => {
		setFetching(false)
		Object.entries(data).length === 0 ? setHasFilteredData(false) : setHasFilteredData(true)
		await props.actions.enterprise.setFilteredEnterprises(data)
	}
	
	const onSuccessModal = async (data) => {
		setModalFetching(false)
		await props.actions.enterprise.setEnterpriseInfo(data)
	}
	
	const onError = (error) => {
		setFetching(false)
		setHasFilteredData(false)
		Alert.alert('Error', 'An error occurred while fetching information from the server.')
	}
	
	const onErrorModal = (error) => {
		setModalFetching(false)
		Alert.alert('Error', 'An error occurred while fetching information from the server.')
	}
	
	return (
		<>
			<View style={styles.searchBarcontainer}>					
				
				<View style={styles.searchBox}>
					<TextInput 
						value={searchText}
						onChangeText={(text) => setSearchText(text)}
						onFocus={() => {setCancelSearch(true)}}
						onSubmitEditing={async () => {
							setCancelSearch(false)
							setFetching(true)
							await props.actions.enterprise.filterEnterprises(searchText, onSuccess, onError)
							hasData ? setHasFilteredData(true) : setHasFilteredData(false) 
						}}
						placeholder={'Search an enterprise...'} 
						placeholderTextColor={stylesConstants.filter.placeholderTextColor} 
						style={styles.textInput}
					/>
					{ cancelSearch && (
						<TouchableOpacity style={styles.clearTextTouchable} onPress={() => setSearchText('')} >
							<Image
								source={cancelIcon}
								style={styles.icon}
							/>
						</TouchableOpacity>
					)}
				</View>
				
				<TouchableOpacity style={styles.searchButtonTouchable} onPress={async () => {
					setCancelSearch(false)
					setFetching(true)
					Keyboard.dismiss()
					await props.actions.enterprise.filterEnterprises(searchText, onSuccess, onError)
					hasData ? setHasFilteredData(true) : setHasFilteredData(false) 
				}}>
					<Image
						source={searchIcon}
						style={styles.icon}
					/>
				</TouchableOpacity>
				
			</View>
			<View style={styles.content}>
				{ fetching ? (
					<ActivityIndicator size={stylesConstants.loading.size} color={stylesConstants.loading.color} />
				) : (
					(hasData) ? (						
						<FlatList
							data={filteredEnterprises}
							showsVerticalScrollIndicator={false}
							keyExtractor={(x, i) => i.toString()}
							renderItem={({ item }) =>
								<TouchableOpacity onPress={async () => { 
									setModalFetching(true)
									await props.actions.enterprise.getEnterpriseInfo(item.id, onSuccessModal, onErrorModal)
									await props.actions.enterprise.setModalVisibility(!modalVisibility) 
								
								}}>	
									<View style={styles.enterprisesContainer}>
										<Text style={styles.textButton}>
											{`${item.enterprise_name}`}
										</Text>
									</View>
								</TouchableOpacity>
							}	
						/>
					) : (
						hasFilteredData ? (
							<TextContent title={textsConstants.filter.initialTitle} description={textsConstants.filter.initialDescription}/>							
						) : (
							<TextContent title={textsConstants.filter.noDataTitle} description={textsConstants.filter.noDataDescription}/>
						)
					)
				)}
			</View>
			<FloatingButton logout={props.logout} />
			<Modal data={enterpriseInfo} fetching={modalFetching}/>
		</>
	)
}

const mapStateToProps = (state) => ({
	data: {
		userInfo: state.login.userInfo,
		customHeader: state.login.customHeader,
		modalVisibility: state.home.modalVisibility,
		enterpriseInfo: state.home.enterpriseInfo,
		filteredEnterprises: state.home.filteredEnterprises
	}
})

const mapDispatchToProps = (dispatch) => ({
	actions: {
		enterprise: bindActionCreators(enterprisesActions, dispatch)
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(Filter)