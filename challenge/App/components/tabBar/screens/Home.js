import React, { Component } from 'react'
import { Text, View } from 'react-native'
import FloatingButton from '../../floatingButton/FloatingButton'
import styles from './styles/HomeStyle'

export default function Home(props){
	const { userInfo } = props.data
	return (
		<>
			<View style={styles.content}>
				<Text style={styles.header}>
					Welcome, {`${userInfo['investor_name']}`}
				</Text>
				<Text style={styles.text}>
					Simple and easy to use, see enterprises through this application
				</Text>
				
			</View>
			<FloatingButton logout={props.logout} />
		</>
	)
}
