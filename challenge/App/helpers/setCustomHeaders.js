import { api } from '../services/api';
import { getCustomHeader } from '../utils/storage';

export default async function setCustomHeaders(){
	const values = await getCustomHeader()
	api.defaults.headers['Content-Type'] = 'application/json'
	api.defaults.headers['access-token'] = values['access-token']
	api.defaults.headers['client'] = values['client']
	api.defaults.headers['uid'] = values['uid']
}