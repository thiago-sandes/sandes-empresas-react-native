import React, { Component } from 'react'
import { Alert } from 'react-native'
import Login from '../../components/login/Login'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as loginActions from '../../actions/loginActions'
import { validate } from '../../utils/validation'
import { saveToken } from '../../utils/storage'
import { NavigationActions, StackActions } from 'react-navigation'

class LoginContainer extends Component {

	state = { isLoading: false }
	onSuccess = (data) => {
		this.setState({ isLoading: false })
		if (data.headers['access-token'] && data.headers['access-token'].length > 0) {
			saveToken(data.headers['access-token']).then((isSuccess) => {
				if (isSuccess) {
					this.props.actions.login.setUserInfo(data.data.investor)
					this.props.actions.login.setCustomHeader(data.headers)
					const { navigation } = this.props
					const resetAction = StackActions.reset({
						index: 0,
						actions: [
							NavigationActions.navigate({ routeName: 'home' })
						]
					})
					navigation.dispatch(resetAction)
				}
			})
		}
	}
	onError = (error) => {
		this.setState({ isLoading: false })
		Alert.alert('Unauthorized access', 'The server did not authorize your access.')
			
	}
	login = (params) => {
		this.setState({ isLoading: true })
		const emailValidation = validate('email', params.email.trim())
		const passwordValidation = validate('password', params.password.trim())

		if (emailValidation.isError) {
			Alert.alert(emailValidation.titleError, emailValidation.messageError)
			this.setState({ isLoading: false })
		} else if (passwordValidation.isError) {
			Alert.alert(passwordValidation.titleError, passwordValidation.messageError)
			this.setState({ isLoading: false })
		} else {
			this.props.actions.login.login({ email: params.email, password: params.password }, this.onSuccess, this.onError)
		}    
	}
		
	render() {
		return (
			<Login
				login={this.login}
				fetching={this.state.isLoading}
			/>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		actions: {
			login: bindActionCreators(loginActions, dispatch)
		}
	}
}

export default connect(null, mapDispatchToProps)(LoginContainer);
